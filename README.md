# Cypress Ecommerce Automation

## Version
1.0.0

## Description
This is an automation testing architecture containing methods and tests for testing online shops. It contains negative and positive scenarios for the following functionalities:
- New user registration
- User login
- Recover a forgotten password
- View and edit user profile information
- Loading a category/subcategory of products
- Add product search filter
- Remove product search filter
- Product search
- Add product to Favorites category
- Add product to the basket
- Remove a product from the basket
- Changing the quantity of a product in the basket
- Enter delivery data
- Choose a payment method
- Ordering a product
- Newsletter subscription
- Display locations of physical stores (if available)
- Contact form request
- Loading social networks
- Change language
- Pagination
The solution could be easily used with no/minimal modifications for variaty of online shops which contains simillar functionalities by just replacing the locators and the data with their own. It is scalable аnd new methods/test could be added by the user. 

## Prerequisites
The code could be installed and used on any operating system which has installed:
- Node.js (preferable version 16.13.1 or higher)
- NPM (preferable version 8.1.2 or higher)
- Cypress (preferable version 12.14.0 or higher)

## Installation
The solution is accessible in a public repository, no authentication is needed. In order to use it, the user needs to do the followig commands in its terminal:
- git clone https://gitlab.com/veliana_krapcheva/cypress-ecommerce-automation.git

## Usage
Open a terminal in the folder where the solution is cloned or open this folder in an IDE (recommend VSCode but any other supporting JavaScript could be used). In the terminal (or the IDE terminal), if the user wants to run all tests in the background, it needs to type the following command:
- npx cypress run
If the user wants to open the Cypress GUI and run all tests or only concrete tests from there, it needs to type the following command in the terminal:
- npx cypress open
For more information about the provided options of Cypress GUI and how to use it, the user can find information here: https://docs.cypress.io/guides/core-concepts/cypress-app 

## Support
In case of any questions, you can receive information by writing to vkrapcheva@gmail.com

## Authors and acknowledgment
Veliana Krapcheva
