/// <reference types="cypress" />

/*
* This class contains methods that will be used in Registration tests
*/
export class RegistrationPage {

  selectAccount(){
    // clicking "my account" button when the user is not logged in
    cy.get('your_locator').click()
  }

  selectRegistrationButton() {
    // clicking Register button
    cy.get('your_locator').click()
  }

  closeBanner(){
    // clicking "close banner" button (if there is such functionality)
    cy.get('your_locator').invoke('remove')
  }

  typeEmail(email){
    // typeing email
    cy.get('your_locator').type(email)
  }

  typeName(name) {
    // typeing name
    cy.get('your_locator').type(name)
  }

  typeSurname(surname) {
    // typeing surname
    cy.get('your_locator').type(surname)
  }

  typePassword(password){
    // typeing password
    cy.get('your_locator').type(password)
  }

  typePasswordConfirmation(password){
    // typeing confirmed password
    cy.get('your_locator').type(password)
  }

  selectDateOfBirth(){
    // selecting day of birth dropdown value
    cy.get('your_locator').select('11')
    // selecting month of birth dropdown value
    cy.get('your_locator').select('04')
    // selecting year of birth dropdown value
    cy.get('your_locator').select('1996')

  }

  selectSubmitRegistrationButton(){
    // clicking submit registration button
    cy.get('your_locator').click();
}

  selectConditionsAccept(){
    // clicking checkbox for accepting terms and conditions
    cy.get('your_locator').click()
  }
  
  selectMarketingAcception(){
    // clicking checkbox for accepting receiving of marketing materials
    cy.get('your_locator').click()
  }

  selectSaveCredentials(){
    // selecting "save credentials" checkbox
    cy.get('locator').click()
  }
}
