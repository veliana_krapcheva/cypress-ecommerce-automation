/// <reference types="cypress" />

/*
* This class contains methods that will be used in Login tests
*/
export class LoginPage {

  selectAccount(){
    // clicking "my account" button when the user is not logged in
    cy.get('yor_locator').click()
  }

  typeUsername(username) {
    // typeing username
    cy.get('your_locator').type(username)
  }

  typeEmail(email){
    // typeing email
    cy.get('your_locator').type(email)
  }

  typePassword(password) {
    // typeing password
    cy.get('your_locator').type(password)
  }

  selectLoginButton(){
    // clicking "login" button
    cy.get('your_locator').click()
  }

  selectSaveCredentials(){
    // clicking "Save/Remember my credentials" checkbox/button
    cy.get('your_locator').click()
  }
  
  selectForgetLogin(){
    // clicking "Forget/reset password" button
    cy.get('your locator').click()
  }

  typeRecoveryEmail(email){
    // typeing recovery email
    cy.get('your_locator').type(email)
  }

  selectSubmitPasswordRecovery(){
    // clicking "submit" button for password recovery
    cy.contains("your_locator").click()
  }
}
