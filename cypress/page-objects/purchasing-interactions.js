/// <reference types="cypress" />

/*
* This class contains methods that will be used in Purchasing Interactions tests
*/
export class PurchasingInteractions {

    openBasket(){
        // clicking "shopping basket" button
        cy.get('your_locator').click()
    }

    changeProductQuantity(){
        // if the quantitiy is a drobdown - clicking button to change the qunatity of the product which is in the basket
        cy.get('your_locator').select('2')
        // if the quantity is a text box - clearing the text field and typeing new value
        cy.get('your_locator').clear().type('2')
        // clicking "update quantity button" (if there is such finctionality)
        cy.get('your_locator').click()

    }

    enterPromoCode(promoCode){
        // clicking button which to display the promo code text area
        cy.get('your_locator').click()
        // typeing promo code
        cy.get('your_locator').type(promoCode)
        // clicking submit promo code button
        cy.get('your_locator').click()
    }

    selectPurchasingFinalizationButton(){
        // clicking purchasing finalization button
        cy.get('your_locator').click()
    }

    selectDeliveryMethod(){
        // clicking radio button to select delivery method
        cy.get('your_locator').click()
    }

    typeDeliveryData(email, name, surname, phoneNumber, city, street, number){
        // Note: this methoud could contain different properties but only the parameters for the concrete properties should be kept in function arguments
        // typeing email
        cy.get('your_locator').type(email)
        // typeing name
        cy.get('your_locator').type(name)  
        //typeing surname
        cy.get('your_locator').type(surname)
        // typeing phone number
        cy.get('your_locator').type(phoneNumber)
        // selecting location for delivery office
        cy.get('your_locator').select('value')
        // typeing city
        cy.get('your_locator').type(city)
        // typeing street
        cy.get('your_locator').type(street)
        // typeing street number
        cy.get('your_locator').type(number)
    }

    selectPaymentMethod(){
        // clicking radio button to select payment method
        cy.get('your_locator').click()
    }

    selectPurchaseButton(){
        // clicking purchase button
        cy.get('your_locator').click()
    }
}
