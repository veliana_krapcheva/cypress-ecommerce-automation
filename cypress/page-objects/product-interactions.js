/// <reference types="cypress" />

/*
* This class contains methods that will be used in Product Interactions tests
*/
export class ProductInterations {

    openProduct(){
        // clicking product which to be opened
        cy.get('your_locator').click()
    }

    selectProductSize(){
        // selecting the size of product which to be added to the basket
        cy.get('your_locator').click()
    }

    addProductToBasket(){
        // clicking "add to basket" button
        cy.get('your_locator').click()
        // clicking "finalizing shopping" button
        cy.get('your_locator').click()
    }

    removeProductFromBasket(){
        // clicking "more options" button for the product in the basket
        cy.get('your_locator').click()
        // clicking "remove product" button
        cy.get('your_locator').click()
    }

    increaseProductQuantity(){
        // clicking button to increase the qunatity of the product which to be added in the basket
        cy.get('your_locator').click()
    }

    decreaseProductQuantity(){
        // clicking button to decrease the quantity of the product which to be added in the basket
        cy.get('your_locator').click()
    }

    checkProductDescription(){
        // clicking button to display description for the product
        cy.get('your_locator').click()
    }

    checkProductQuantityInShops(){
        // clicking "check quantity in stores" button
        cy.get('your_locator').click()
        // typeing postal code of the city in which the stores to be cheched
        cy.get('your_locator').type('1000')
    }

    addProductToWishList(){
        // clicking "add product to favourites" button
        cy.get('your_locator').click()
    }
}
