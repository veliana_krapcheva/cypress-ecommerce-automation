/// <reference types="cypress" />

/*
* This class contains methods that will be reusable in all tests
*/
export class CommonPage {

  navigate() {
      cy.visit(Cypress.env('base_url'))
    }

    acceptCookie(){
      // clicking "accept cookie" button
      cy.get('your_locator').click()
    }
    
    login(username, password){
      // clicking "my account" button when the user is not logged in
      cy.get('your_locator').click()
      // typeing username
      cy.get('your_locator').type(username)
      // typeing passwotd
      cy.get('your_locator').type(password)
      // clicking "login" button
      cy.get('your_locator').click()
    }

    selectLoggedAccount(){
      // clicking "my account" button when the user is logged in
      cy.get('your_locator').click()
    }

    logout(){
      // clicking "my account" button when the user is logged in
      cy.get('your_locator').click()
      // clicking "logout" button
      cy.get('your_locator').click()
    }
}
