/// <reference types="cypress" />

/*
* This class contains methods that will be used in Marketing and Contacts tests
*/
export class MarketingAndContacts {

    subscribeForBulletine(email){
        // typeing email in bulletine subscription text box
        cy.get('your_locator').type(email)
        // clicking "accepting conditions" checkbox (if such object does not exist, remove the line below)
        cy.get('your_locator').click()
        // clicking "submit" button   
        cy.get('your_locator').click()
    }

    selectViewShops(){
        // clicking "view all shops" button
        cy.get('you_locator').click()
    }

    selectContactsButton(){
        // clicking "contacts" button
        cy.get('your_locator').click()
    }

    selectQuestionCategory(){
        // select question category button to display the request form button
        cy.get('your_locator').click()
    }

    selectContactFormRequesting(){
        // clicking "write us" button
        cy.get('your_locator').click()
    }

    fillContactForm(name, surname, email){
        // typeing first name in the contact request form
        cy.get('your_locator').type(name)
        // typeing last name in the contact request form        
        cy.get('your_locator').type(surname)
        // typeing email or phone in the contact request form        
        cy.get('your_locator').type(email)
        // typeing order number in the contact request form (if such object does not exist, remove the line below)
        cy.get('your_locator').click()
        // clicking department dropdown and selecting department (if such object does not exist, remove the line below)
        cy.contains('your_locator').click()
        // clicking title dropdown and selecting title (if such object does not exist, remove the line below)
        cy.get('your_locator').click()
        // typeing text of the message
        cy.get('your_locator').type('your_text')
    }

    selectSendButton(){
        // clicking send contact request button
        cy.get('your_locator').click()
    }

    selectSocialMediaOpening() {
        // clicking social media button
        cy.get('your_locator').invoke('removeAttr','target').click()
      }      
}
