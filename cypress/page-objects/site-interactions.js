/// <reference types="cypress" />

/*
* This class contains methods that will be used in Site Interactions tests
*/
export class SiteInteractions {

    selectMenu(){
        // clicking button menu to be opened
        cy.get('your_locator').click()        
    }

    selectCategory(){
        // clicking button category to be opened
        cy.get('your_locator').click()
    }

    selectSubCategory(){
        // clicking button subcategory to be opened
        cy.get('your_locator').click();
    }

    searchProduct(productNumber){
        // clicking area for typeing product which to be searched
        cy.get('your_locator').click()
        // typeing product which to be searched
        cy.get('your_locator').type(productNumber+'{enter}')
    }

    setFilter(){
        // perform actions to set product serch filter. The implementation method for this functionality is quite different that's why the user needs to customize it, that's why only 3 clicking button methods are left as a reference
        cy.get('your_locator').click()
        cy.get('your_locator').click()
        cy.get('your_locator').click()
    }

    cleanFilter(){
        // clicking clean search filter button
        cy.get('your_locator').contains('Изчистване на филтрите').click()
    }

    returnFilterResults(){
        // clicking button for retrieving products for a search filter
        cy.get('your_locator').click()
    }

    updateProfile(newName){
        // typeing the updated name
        cy.get('your_locator').type(newName)
        // clicking submit button
        cy.get('your_locator').click()
    }

    changeLanguage(){
        // clicking "language" icon to show all available languages
        cy.get('your_locator').click()
        // selecting language
        cy.get('your_locator').click()
    }

    checkBanner(){
        // checking if a banner is visible
        cy.get('your_locator').should('be.visible')
    }

    selectSecondPage(){
        // clicking button to go to page 2
        cy.get('your_locator').click()
    }    
}
