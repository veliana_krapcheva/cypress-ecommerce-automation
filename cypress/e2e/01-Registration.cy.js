/// <reference types="cypress" />

import {RegistrationPage} from '../page-objects/registration'
import {CommonPage} from '../page-objects/common'

describe('Registration Tests', () => {
    const commonPage = new CommonPage()
    const registrationPage = new RegistrationPage()

    beforeEach(() => {                
        commonPage.navigate()
        commonPage.acceptCookie()
    })

    it('Unsuccessful Registration - not valid password', () => {
        registrationPage.selectAccount()
        registrationPage.selectRegistrationButton()
        registrationPage.typeEmail(Cypress.env('email'))
        registrationPage.typeName(Cypress.env('name'))
        registrationPage.typeSurname(Cypress.env('surname'))
        registrationPage.typePassword("123")
        registrationPage.typePasswordConfirmation("123")        
        registrationPage.selectDateOfBirth()        
        registrationPage.selectConditionsAccept()
        registrationPage.selectSubmitRegistrationButton()
        cy.contains('your_text').should('be.visible')
    })

    it('Unsuccessful Registration - not valid email', () => {
        registrationPage.selectAccount()
        registrationPage.selectRegistrationButton()
        registrationPage.typeEmail("test")
        registrationPage.typeName(Cypress.env('name'))
        registrationPage.typeSurname(Cypress.env('surname'))
        registrationPage.typePassword(Cypress.env('password'))
        registrationPage.typePasswordConfirmation(Cypress.env('password'))      
        registrationPage.selectDateOfBirth()        
        registrationPage.selectConditionsAccept()
        registrationPage.selectSubmitRegistrationButton()
        cy.contains('your_text').should('be.visible')
    })

    it('Successful Registration', () => {
        registrationPage.selectAccount()
        registrationPage.selectRegistrationButton()
        registrationPage.typeEmail(Cypress.env('email'))
        registrationPage.typeName(Cypress.env('name'))
        registrationPage.typeSurname(Cypress.env('surname'))
        registrationPage.typePassword(Cypress.env('password'))
        registrationPage.typePasswordConfirmation(Cypress.env('password'))      
        registrationPage.selectDateOfBirth()        
        registrationPage.selectConditionsAccept()
        registrationPage.selectSubmitRegistrationButton()
        cy.get('your_locator').should('be.visible')
        registrationPage.selectLoggedAccount()
        cy.get('your_locator').should('be.visible')
    })

    it('Successful Registration - Accepting Marketing Messages', () => {
        registrationPage.selectAccount()
        registrationPage.selectRegistrationButton()
        registrationPage.typeEmail(Cypress.env('email'))
        registrationPage.typeName(Cypress.env('name'))
        registrationPage.typeSurname(Cypress.env('surname'))
        registrationPage.typePassword(Cypress.env('password'))
        registrationPage.typePasswordConfirmation(Cypress.env('password'))      
        registrationPage.selectDateOfBirth()        
        registrationPage.selectConditionsAccept()
        registrationPage.selectMarketingAcception()
        registrationPage.selectSubmitRegistrationButton()
        cy.get('your_locator').should('be.visible')
        registrationPage.selectLoggedAccount()
        cy.get('your_locator').should('be.visible')
    })

    it('Unsuccessful Registration - already taken email/username', () => {
        registrationPage.selectAccount()
        registrationPage.selectRegistrationButton()
        registrationPage.typeEmail(Cypress.env('email'))
        registrationPage.typeName(Cypress.env('name'))
        registrationPage.typeSurname(Cypress.env('surname'))
        registrationPage.typePassword(Cypress.env('password'))
        registrationPage.typePasswordConfirmation(Cypress.env('password'))      
        registrationPage.selectDateOfBirth()        
        registrationPage.selectConditionsAccept()
        registrationPage.selectSubmitRegistrationButton()
        cy.contains('your_text').should('be.visible')

    })

    it('Unsuccessful Registration - not matching passwords', () => {
        registrationPage.selectAccount()
        registrationPage.selectRegistrationButton()
        registrationPage.typeEmail(Cypress.env('email'))
        registrationPage.typeName(Cypress.env('name'))
        registrationPage.typeSurname(Cypress.env('surname'))
        registrationPage.typePassword(Cypress.env('password'))
        registrationPage.typePasswordConfirmation('different_password')     
        registrationPage.selectDateOfBirth()        
        registrationPage.selectConditionsAccept()
        registrationPage.selectSubmitRegistrationButton()
        cy.get('your_locator').should('be.visible')
        registrationPage.selectLoggedAccount()
        cy.get('your_locator').should('be.visible')
    })

})
