/// <reference types="cypress" />

import {ProductInterations} from '../page-objects/product-interactions'
import {CommonPage} from '../page-objects/common'
import {SiteInteractions} from '../page-objects/site-interactions'
import {PurchasingInteractions} from '../page-objects/purchasing-interactions.js'

describe('Purchasing Interactions Tests', () => {

    const commonPage = new CommonPage()
    const productInterations = new ProductInterations()
    const siteInteraction = new SiteInteractions()
    const purchasingInteractions = new PurchasingInteractions()

    beforeEach(() => {                
        commonPage.navigate()
        commonPage.acceptCookie()
        commonPage.login(Cypress.env('email'), Cypress.env('password'))
        cy.get('[your_locator').should('be.visible')
    })

    it('Change product qunatity', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.addProductToBasket()
        cy.wait(1000)       
        purchasingInteractions.openBasket()
        purchasingInteractions.changeProductQuantity()
        cy.get('your_locator').should('contain', '2');
    })

    it('Enter promo code', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.addProductToBasket()
        cy.wait(1000)       
        purchasingInteractions.openBasket()
        purchasingInteractions.enterPromoCode(Cypress.env('invalidPromoCode'))
        cy.get('your_locator').should('be.visible')
    })

    it('Purchase the products in the basket', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.addProductToBasket()
        cy.wait(1000)       
        purchasingInteractions.openBasket()
        purchasingInteractions.selectPurchasingFinalizationButton()
        purchasingInteractions.selectDeliveryMethod()
        purchasingInteractions.typeDeliveryData(Cypress.env('email'), Cypress.env('name'), Cypress.env('surname'), 
            (Cypress.env('phoneNumber'), Cypress.env('city'), Cypress.env('street'), Cypress.env('number')))
        purchasingInteractions.selectPaymentMethod()
        purchasingInteractions.confirmTerms()
        purchasingInteractions.selectPurchaseButton()
        cy.get('your_locator').should('be.visible')
    })
})
