/// <reference types="cypress" />

import {LoginPage} from '../page-objects/login'
import {CommonPage} from '../page-objects/common'

describe('Login Tests', () => {
    const commonPage = new CommonPage()
    const loginPage = new LoginPage()

    beforeEach(() => {                
        commonPage.navigate()
        commonPage.acceptCookie()
    })

    it('Unsuccessful Login - empty password', () => {
        loginPage.selectAccount()
        loginPage.typeEmail(Cypress.env('email'))
        loginPage.selectLoginButton()
        cy.contains('your_text').should('be.visible')
    })

    it('Unsuccessful Login - wrong password', () => {
        loginPage.selectAccount()
        loginPage.typeEmail(Cypress.env('email'))
        loginPage.typePassword("wrongpass")
        loginPage.selectLoginButton()
        cy.contains('your_text').should('be.visible')
    })

    it('Unsuccessful Login - incorrect username/user email', () => {
        loginPage.selectAccount()
        loginPage.typeEmail("wronguser@abv.bg")
        loginPage.typePassword(Cypress.env('password'))
        loginPage.selectLoginButton()
        cy.contains('your_text').should('be.visible')
    })

    it('Forgot Password', () => {
        loginPage.selectAccount()
        loginPage.selectForgetLogin()
        loginPage.typeRecoveryEmail(Cypress.env('email'))
        loginPage.selectSubmitPasswordRecovery()
        cy.contains('your_text' + Cypress.env('email') + 'your_text').should('be.visible')        
    })

    it('Successful Login', () => {
        loginPage.selectAccount()
        loginPage.typeEmail(Cypress.env('email'))
        loginPage.typePassword(Cypress.env('password'))
        loginPage.selectLoginButton()
        commonPage.selectLoggedAccount()
        cy.get('your_locator').should('be.visible')
    })

    it('Successful Login with Saved Credentials', () => {
        loginPage.selectAccount()
        loginPage.typeEmail(Cypress.env('email'))
        loginPage.typePassword(Cypress.env('password'))
        loginPage.selectSaveCredentials()
        loginPage.selectLoginButton()
        commonPage.selectLoggedAccount()
        cy.get('your_locator').should('be.visible')
    })

})
