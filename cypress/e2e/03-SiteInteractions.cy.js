/// <reference types="cypress" />

import {CommonPage} from '../page-objects/common'
import {SiteInteractions} from '../page-objects/site-interactions'

describe('Site Interactions Tests', () => {

    const commonPage = new CommonPage()
    const siteInteraction = new SiteInteractions()

    beforeEach(() => {                
        commonPage.navigate()
        commonPage.acceptCookie()
        commonPage.login(Cypress.env('email'), Cypress.env('password'))
        cy.get('your_locator').should('be.visible')

    })
    it('Loading Category', () => {
        siteInteraction.selectMenu()
        siteInteraction.selectCategory()
        siteInteraction.selectSubCategory()
        cy.contains('your_text').should('be.visible')
    })

    it('Searching for existing product', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        cy.contains('your_text').should('be.visible');
    })

    it('Searching for non-existing product', () => {
        siteInteraction.searchProduct(Cypress.env('nonExistingProductNumber'))
        cy.contains('your_text').should('be.visible');
    })

    it('Adding search filter', () => {
        siteInteraction.searchProduct(Cypress.env('productToFilter'))
        siteInteraction.setFilter()
        cy.get('your_locator').should('contain', 'your_value')
    })

    it('Retrieving results from filter', () => {
        siteInteraction.searchProduct(Cypress.env('productToFilter'))
        siteInteraction.setFilter()
        siteInteraction.returnFilterResults()
        cy.get('your_locator').should('exist')
        })

    it('Cleaning search filter', () => {
        siteInteraction.searchProduct(Cypress.env('productToFilter'))
        siteInteraction.setFilter()
        siteInteraction.cleanFilter()
        cy.get('your_locator').should('not.exist')
    })

    it('Pagination: Go on next page', () => {
        siteInteraction.selectMenu()
        siteInteraction.selectCategory()
        siteInteraction.selectSubCategory()
        siteInteraction.selectSecondPage()
        cy.contains('your_text').should('be.visible')
        
    })
    
    it('Edit profile information', () => {
        commonPage.selectLoggedAccount()
        siteInteraction.updateProfile(Cypress.env('updatedFirstName'))
        cy.contains('your_text').should('be.visible')
    })

    it('Change language', () => {
        siteInteraction.selectMenu()
        siteInteraction.changeLanguage()
        cy.contains('your_text').should('be.visible')
    })

    it('Check temporary banner is visible', () => {
        siteInteraction.checkBanner()
    })

    it('Exit', () => {
        commonPage.selectLoggedAccount()   
        commonPage.logout()     
        cy.contains('your_text').click()      
    })
    
})
