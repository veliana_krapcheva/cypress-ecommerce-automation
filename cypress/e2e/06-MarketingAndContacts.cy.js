/// <reference types="cypress" />

import {CommonPage} from '../page-objects/common'
import {MarketingAndContacts} from '../page-objects/marketing-and-contacts'

describe('Marketing and Contacts Tests', () => {

    const commonPage = new CommonPage()
    const marketingContacts = new MarketingAndContacts()

    beforeEach(() => {                
        commonPage.navigate()
        commonPage.acceptCookie()
    })

    it('Subscribe for bulletine', () => {
        marketingContacts.subscribeForBulletine(Cypress.env('newsLetterMail'))
        cy.get('your_locator').should('be.visible')
    })

    it('View shops', () => {
        marketingContacts.selectViewShops()
        cy.get('your_locator').should('be.visible')

    })

    it('Contact form request', () => {
        marketingContacts.selectContactsButton()
        marketingContacts.selectQuestionCategory()
        marketingContacts.selectContactFormRequesting()
        marketingContacts.fillContactForm("test", "test", "test")
        marketingContacts.selectSendButton()
        cy.contains('your_text').should('be.visible')
    })

    it('Loading social media', () => {
        marketingContacts.selectSocialMediaOpening()
        cy.origin('https://www.facebook.com', () => {
        cy.contains('Allow the use of cookies from Facebook on this browser?')
        })
      })
})
