/// <reference types="cypress" />

import {ProductInterations} from '../page-objects/product-interactions'
import {CommonPage} from '../page-objects/common'
import {SiteInteractions} from '../page-objects/site-interactions'

describe('Product Interactions Tests', () => {

    const commonPage = new CommonPage()
    const productInterations = new ProductInterations()
    const siteInteraction = new SiteInteractions()

    beforeEach(() => {                
        commonPage.navigate()
        commonPage.acceptCookie()
    })

    it('Add product to favorurites', () => {            
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))     
        productInterations.openProduct()
        productInterations.addProductToWishList()       
        cy.contains('your_text').should('be.visible')       
    })

    it('Add product to basket', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.addProductToBasket()
        cy.get('your_locator').should('be.visible')
        })

    it('Remove product from basket', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.addProductToBasket()
        productInterations.openBasket()
        productInterations.removeProductFromBasket()
        cy.contains('your_text').should('be.visible')
    })

    it('Changing product quantity', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.increaseProductQuantity()
        cy.contains('your_text').should('be.visible')
    })

    it('Check product quantity in shops', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.selectProductSize()
        productInterations.checkProductQuantityInShops()
        cy.contains('your_text').should('be.visible')
    })

    it('Check product description', () => {
        siteInteraction.searchProduct(Cypress.env('existingProductNumber'))
        productInterations.openProduct()
        productInterations.checkProductDescription()
        cy.get('your_locator').should(($div) => {
            expect($div.text().trim()).to.not.be.empty;
  })
})
})
